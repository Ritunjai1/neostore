import React, { Component } from "react";
import "./demo.css";

class Demo extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div class="gallery js-flickity">
          <div class="gallery-cell" />
          <div class="gallery-cell" />
          <div class="gallery-cell" />
          <div class="gallery-cell" />
          <div class="gallery-cell" />
          <div class="gallery-cell" />
          <div class="gallery-cell" />
        </div>
      </div>
    );
  }
}
export default Demo;
