import React, { Component } from "react";
import "./products.css";
class Products extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h4>
          Popular Products <a href="#">---view all</a>
        </h4>
        <br />
        <div>
          <div
            id="multi-item-example"
            class="carousel slide carousel-multi-item"
            data-ride="carousel"
          >
            <div class="controls-top">
              <a
                class="btn-floating"
                href="#multi-item-example"
                data-slide="prev"
              >
                <i class="fas fa-chevron-left" />
              </a>
              <a
                class="btn-floating"
                href="#multi-item-example"
                data-slide="next"
              >
                <i class="fas fa-chevron-right" />
              </a>
            </div>

            <ol class="carousel-indicators">
              <li
                data-target="#multi-item-example"
                data-slide-to="0"
                class="active"
              />
              <li data-target="#multi-item-example" data-slide-to="1" />
              <li data-target="#multi-item-example" data-slide-to="2" />
            </ol>

            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="carousel-item">
                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(60).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="carousel-item">
                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="card mb-2">
                    <img
                      class="card-img-top"
                      src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
                      alt="Card image cap"
                    />
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a class="btn btn-primary">Button</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="card-deck">
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/thumbnail/_Twain_Study_table_00_250thumb.jpg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  {" "}
                  <h5 className="card-title">Twain Study Table</h5>
                </a>
                <h6>₹5,000</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star " />
              </div>
            </div>
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/thumbnail/_15b-lightgold-oak-birch-batula-vintage-light-gold-3-2-mahogany-original-imaeenhryfgb6ns4_250thumb.jpeg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  <h5 className="card-title">Recliner Sofa Set</h5>
                </a>
                <h6>₹30,000</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star " />
                <span className="fa fa-star " />
                <span className="fa fa-star " />
              </div>
            </div>
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/image/DANUM_SWING_CHAIR_01.jpg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  <h5 className="card-title">Danum Swing Chair</h5>
                </a>
                <h6>₹12,000</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
              </div>
            </div>
          </div>
        </div>
        <br />
        <div>
          <div className="card-deck">
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/thumbnail/_FNSF51WCCO3_-_main_2_250thumb.jpg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  {" "}
                  <h5 className="card-title">Godrej</h5>
                </a>
                <h6>₹15,000</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star " />
                <span className="fa fa-star " />
              </div>
            </div>
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/thumbnail/_Apollo_Infinite_FNSF51APPL3S000SAAAA_slide_03_250thumb.jpg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  <h5 className="card-title">Robbinson Sofa</h5>
                </a>
                <h6>₹6,000</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star " />
                <span className="fa fa-star " />
              </div>
            </div>
            <div className="card">
              <img
                className="card-img-top"
                src="http://180.149.241.208:3000/storage/image/Cooper_Rocker_Recliner_Brown_01_2.jpg"
                alt="Card image cap"
              />
              <div className="card-body">
                <a href="#">
                  <h5 className="card-title">Cooper Rocker Recliner</h5>
                </a>
                <h6>₹2,500</h6>
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star checked" />
                <span className="fa fa-star " />
                <span className="fa fa-star " />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Products;
