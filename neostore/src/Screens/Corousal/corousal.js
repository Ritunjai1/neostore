import React, { Component } from "react";
import "./corousal.css";
class Corousal extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ol className="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            className="active"
          />
          <li data-target="#carouselExampleIndicators" data-slide-to="1" />
          <li data-target="#carouselExampleIndicators" data-slide-to="2" />
          <li data-target="#carouselExampleIndicators" data-slide-to="3" />
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              className="d-block w-100"
              src="https://www.ulcdn.net/images/products/169926/original/FNSF51EAGY3_-_main_1.jpg?1519884687"
              alt="First slide"
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://hivemodern.com/public_resources/neo-sectional-niels-bendtsen-bensen-1.jpg"
              alt="Second slide"
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://cdn11.bigcommerce.com/s-1u1m3wn/images/stencil/800x722/products/2900/5347/Daylesford-7-piece-dining-with-matching-grey-benson-fabric-chairs-lifestyle__30735.1537336665.jpg?c=2"
              alt="Third slide"
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://cdn.shopify.com/s/files/1/2660/5202/products/yrj4fpg2cnwexjryligk_ecb18b2e-fa53-47b3-a7cd-718e49ef4438_800x.jpg?v=1536592934"
              alt="Fourth slide"
            />
          </div>
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}
export default Corousal;
