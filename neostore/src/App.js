import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header/header";
import Corousal from "./Screens/Corousal/corousal";
import Products from "./Screens/products/products";
import Footer from "./components/Footer/footer";
// import Demo from "./demo";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <br />
        <Corousal />
        <br />
        <Products />
        <br />
        <Footer />
      </div>
    );
  }
}

export default App;
