import React, { Component } from "react";
import "./footer.css";

class Footer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div className="footer">
          <div className="container">
            <div className="row">
              <div className="col-4">
                <h5>About Company</h5>
                <p>
                  NeoSOFT Technologies is here at your quick <br />
                  and easy service for shooping .<br />
                  Contact information
                  <br />
                  Email: contact@neosofttech.com <br />
                  Phone: +91 0000000000 <br />
                  MUMBAI, INDIA
                </p>
              </div>
              <div className="col-4">
                <h5>Information</h5>
                <a href="#">Terms and condition</a>
                <br />
                <a href="#">Guarantee and Reurn Policy</a>
                <br />
                <a href="#">Contact Us</a>
                <br />
                <a href="#">Privacy Policy</a>
                <br />
                <a href="#">Locate Us</a>
              </div>
              <div className="col-4">
                <h5>NewsLetter</h5>
                <p>
                  Sign up to get exclusive offers from our <br />
                  favorite brands and to be well up in the news.
                  <br />
                  <input type="email" value="your email" />
                  <br />
                  <br />
                  <button>Subscribe</button>
                </p>
              </div>
            </div>
            <div className="row">
              <p id="copyright">
                Copyright © 2017 NeoSOFT Technologies All rights reserved |
                Design by <a href="#">NeoSOFT Technologies</a>
              </p>
            </div>
            <br />
          </div>
        </div>
      </div>
    );
  }
}
export default Footer;
